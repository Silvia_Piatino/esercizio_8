# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#define COSTANTE 3600

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;

BusStation::BusStation(const string &busFilePath) {
    _busFilePath=busFilePath;
}


void BusStation::Load(){
    _buses.clear();
    ifstream file;
    file.open(_busFilePath);
    if (file.fail())
        throw runtime_error("Something goes wrong");
    try{
        string line;
        getline(file, line);
        getline(file, line);

        int numberBuses=std::stoi(line); //viene deallocato appena load termina

        getline(file, line);
        getline(file, line);

        for(int b=0; b<numberBuses; b++)
        {
            getline(file, line);
            int key = stoi(line.substr(0, line.find(" "))); //find ti dice dove si trova nella stringa
            int fuel = stoi(line.substr(line.find(" ")+1, line.length())); //substr identifica la parte di linea che parte dal primo spazio+1 fino alla fine e stoi la converte in intero
            Bus busToAdd = Bus(); //creo un oggetto bus vuoto e lo riempio e poi lo aggiungo
            busToAdd.Id=key;
            busToAdd.FuelCost=fuel;
            _buses[key]=busToAdd;
        }
    }
    catch(exception){
        _buses.clear();
        throw runtime_error("Something goes wrong");
    }
}

int BusStation::NumberBuses() const  { return _buses.size(); }


const Bus &BusStation::GetBus(const int &idBus) const {
    if(_buses.count(idBus)==0)
        throw runtime_error("Bus " + to_string(idBus) + " does not exists");
    return _buses.at(idBus);

}

MapData::MapData(const string &mapFilePath) {
    _mapFilePath=mapFilePath;
}

void MapData::Load() {
    _routes.clear();
    _streets.clear();
    _busStops.clear();
    _routeDescription.clear();

    ifstream file;
    file.open(_mapFilePath);
    if (file.fail())
        throw runtime_error("Something goes wrong");

    try{
        string line;
        getline(file, line);
        getline(file, line);

        int numberBusStops=std::stoi(line); //viene deallocato appena load termina

        getline(file, line);

        for(int b=0; b<numberBusStops; b++)
        {
            getline(file, line);
            istringstream iss(line);
            vector<string> results(istream_iterator<string>{iss}, istream_iterator<string>());
            BusStop busStopToAdd = BusStop();
            busStopToAdd.Id = stoi (results[0]);
            busStopToAdd.Name = results[1];
            busStopToAdd.Latitude = stoi (results[2]);
            busStopToAdd.Longitude = stoi (results[3]);

            _busStops[busStopToAdd.Id]=busStopToAdd;
        }

        getline(file, line);
        getline(file, line);

        int numberStreets=std::stoi(line);

        getline(file, line);

        for(int s=0; s<numberStreets; s++)
        {
            getline(file, line);
            istringstream iss(line);
            vector<string> results(istream_iterator<string>{iss}, istream_iterator<string>());
            Street streetToAdd = Street();
            pair<int, int> pairToAdd(stoi(results[1]), stoi(results[2])); //crea la coppia From To da aggiungere
            streetToAdd.Id = stoi (results[0]);
            streetToAdd.TravelTime = stoi (results[3]);

            _streets[streetToAdd.Id]=streetToAdd;
            _streetDescription[streetToAdd.Id]=pairToAdd;
        }

        getline(file, line);
        getline(file, line);

        int numberRoutes=std::stoi(line);

        getline(file, line);

        for(int r=0; r<numberRoutes; r++)
        {
            getline(file, line);
            istringstream iss(line);
            vector<string> results(istream_iterator<string>{iss}, istream_iterator<string>());
            Route routeToAdd = Route();
            vector<int> vectorToAdd; //crea
            routeToAdd.Id = stoi (results[0]);
            routeToAdd.NumberStreets = stoi (results[1]); //mi dice quanti id delle strade devo aggiungere al vettore

            for(int i=0; i<routeToAdd.NumberStreets; i++)
                vectorToAdd.push_back(stoi(results[2+i]));

            _routes[routeToAdd.Id]=routeToAdd;
            _routeDescription[routeToAdd.Id]=vectorToAdd;
        }

    }
    catch(exception){
        _routes.clear();
        _streets.clear();
        _busStops.clear();
        _routeDescription.clear();
        throw runtime_error("Something goes wrong");
    }

}
int MapData::NumberRoutes() const { return _routes.size(); }

int MapData::NumberStreets() const { return _streets.size(); }

int MapData::NumberBusStops() const { return _busStops.size(); }

const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const {
    if(_routes.count(idRoute)==0)
        throw runtime_error("Route " + to_string(idRoute) + " does not exists");
    if (streetPosition>=_routes.at(idRoute).NumberStreets)
        throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");
    return _streets.at(_routeDescription.at(idRoute)[streetPosition]);
}

const Route &MapData::GetRoute(const int &idRoute) const {
    if(_routes.count(idRoute)==0)
        throw runtime_error("Route " + to_string(idRoute) + " does not exists");
    return _routes.at(idRoute);
}

const Street &MapData::GetStreet(const int &idStreet) const {
    if(_streets.count(idStreet)==0)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");
    return _streets.at(idStreet);
}

const BusStop &MapData::GetStreetFrom(const int &idStreet) const {
    if(_streets.count(idStreet)==0)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");
    return _busStops.at(_streetDescription.at(idStreet).first);
}

const BusStop &MapData::GetStreetTo(const int &idStreet) const {
    if(_streets.count(idStreet)==0)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");
    return _busStops.at(_streetDescription.at(idStreet).second);
}

const BusStop &MapData::GetBusStop(const int &idBusStop) const {
    if(_busStops.count(idBusStop)==0)
        throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");
    return _busStops.at(idBusStop);
}

int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const {
    int totalTravelTime=0;
    for(int i=0;i<_mapData.GetRoute(idRoute).NumberStreets; i++)
        totalTravelTime+=_mapData.GetRouteStreet(idRoute, i).TravelTime;
    return totalTravelTime;
}

int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const {
    int totalRouteCost=0;
    for(int i=0;i<_mapData.GetRoute(idRoute).NumberStreets; i++)
        totalRouteCost+=(_mapData.GetRouteStreet(idRoute, i).TravelTime*_busStation.GetBus(idBus).FuelCost*BusAverageSpeed);//non metto qua la costante perché essendo totalRouteCost un int, dividendo per 3600 il compilatore tronca i decimali->no precisione
    return totalRouteCost/COSTANTE;
}

string MapViewer::ViewRoute(const int &idRoute) const {
    const Route& route = _mapData.GetRoute(idRoute);

    //concatenazione di tutte le fermate
    ostringstream routeView;
    routeView<< route.Id<< ": ";
    for(int s=0; s<route.NumberStreets; s++)
    {
        const Street& street = _mapData.GetRouteStreet(idRoute, s); //prendo la strada; const perché la ViewRoute è const
        const BusStop& from= _mapData.GetStreetFrom(street.Id);//prendo i nomi della fermata From; prendo referenza alla fermata From
        routeView<<from.Name<< " -> "; //concateno

        if(s==route.NumberStreets -1) //se siamo nell'ultima strada allora prendiamo il to
        {
            const BusStop& to = _mapData.GetStreetTo(street.Id);
            routeView<< to.Name;
        }
    }

    return routeView.str();
}

string MapViewer::ViewStreet(const int &idStreet) const {
    //prendo la street->sfrutto l'interfaccia del database
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    const BusStop& to = _mapData.GetStreetTo(idStreet);

    return to_string(idStreet) +
            ": " +
            from.Name +
            " -> " +
            to.Name;
}

string MapViewer::ViewBusStop(const int &idBusStop) const {
    //prendo il busStop->sfrutto l'interfaccia del database
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);

    return busStop.Name +
            " (" +
            to_string((double)busStop.Latitude / 10000.0) +
            ", " +
            to_string((double)busStop.Longitude / 10000.0) +
            ")";
}

}
