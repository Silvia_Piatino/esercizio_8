#ifndef VIAMICHELIN_H
#define VIAMICHELIN_H

#include <iostream>
#include <exception>

#include "bus.h"
#include <vector>
#include <unordered_map>


using namespace std;
using namespace BusLibrary;

namespace ViaMichelinLibrary {

  class BusStation : public IBusStation {
    private:
      string _busFilePath;
      unordered_map<int, Bus> _buses; //chiavi intere, valori bus

    public:
      BusStation(const string& busFilePath);

      void Load();
      int NumberBuses() const;
      const Bus& GetBus(const int& idBus) const;
  };

  class MapData : public IMapData {
    private:
      string _mapFilePath;
      //uso le mappe anche se l'id viene usato sia come chiave sia come uno dei valori perché quando uso getid la complessità computazionale è minore
      unordered_map<int, Route> _routes;
      unordered_map<int, Street> _streets;
      unordered_map<int, BusStop> _busStops;
      unordered_map<int, vector<int>> _routeDescription; //contiene dato id route, l'elenco di strade del percorso
      unordered_map<int, pair<int, int>> _streetDescription; //associa l'id strada agli id delle fermate di partenza e arrivo

    public:
      MapData(const string& mapFilePath);
      void Load();
      int NumberRoutes() const;
      int NumberStreets() const;
      int NumberBusStops() const;
      const Street& GetRouteStreet(const int& idRoute, const int& streetPosition) const;
      const Route& GetRoute(const int& idRoute) const;
      const Street& GetStreet(const int& idStreet) const;
      const BusStop& GetStreetFrom(const int& idStreet) const;
      const BusStop& GetStreetTo(const int& idStreet) const;
      const BusStop& GetBusStop(const int& idBusStop) const;
  };

  class RoutePlanner : public IRoutePlanner {
    public:
      static int BusAverageSpeed;
    private:
        const IMapData& _mapData;
        const IBusStation& _busStation;

    public:
      RoutePlanner(const IMapData& mapData,
                   const IBusStation& busStation): _mapData(mapData), _busStation(busStation){}

      int ComputeRouteTravelTime(const int& idRoute) const;
      int ComputeRouteCost(const int& idBus, const int& idRoute) const;
  };

  class MapViewer : public IMapViewer {
    private:
        const IMapData& _mapData;
    public:
      MapViewer(const IMapData& mapData): _mapData(mapData) { }
      string ViewRoute(const int& idRoute) const;
      string ViewStreet(const int& idStreet) const;
      string ViewBusStop(const int& idBusStop) const;
  };
}

#endif // VIAMICHELIN_H
